import React from 'react';
import './AppView.css';

import Appointments from '../Appointments/Appointments';

function AppView(props) {
  return (
    <div className="AppView">
      <Header {...props} />
      <Appointments {...props} />
      <Footer {...props} />
    </div>
  );
}

function Header(props) {
  return (
    <header id="header">
      <h1>Appointments</h1>
    </header>
  );
}

function Footer(props) {
  if (props.appointments.size === 0) {
    return null;
  }

  const remaining = props.appointments.filter(appointment => !appointment.occupied).size;

  return (
    <footer id="footer">
      <span id="appointment-count">
        <strong>
          {remaining}
        </strong>
        {' appointment(s) left'}
      </span>
    </footer>
  );
}

export default AppView;
