import React, { Component } from 'react';
import './Appointments.css';

import closeIcon from '../../assets/close.png';

class Appointments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      name: "",
      phone: "",
      timeSlot: "",
      showModal: false,
      modalError: false
    };
  }

  handleApptClick = (e) => {
    let apptId = e.currentTarget.dataset.apptId,
        apptName = e.currentTarget.dataset.apptName,
        apptPhone = e.currentTarget.dataset.apptPhone,
        apptTimeSlot = e.currentTarget.dataset.apptTimeslot;

    this.setState((prevState, props) => {

      return {
        id: apptId,
        name: apptName,
        phone: apptPhone,
        timeSlot: apptTimeSlot,
        showModal: !prevState.showModal
      };
    });
  }

  handleNameChange = (e) => {
    this.setState({
      name: e.target.value
    });
  }

  handlePhoneChange = (e) => {
    this.setState({
      phone: e.target.value
    });
  }

  handleSubmit = () => {
    let phoneRegex = /^(\+0?1\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/;

    if (this.state.phone === "" || phoneRegex.test(this.state.phone)) {

      this.props.onUpdate(this.state.id, this.state.name, this.state.phone);

      this.setState((prevState, props) => {
        return {
          modalError: false,
          showModal: !prevState.showModal
        };
      });
    }

    else {
      this.setState({ modalError: true });
    }
  }

  toggleModal = () => {
    this.setState((prevState, props) => {
      return {
        modalError: false,
        showModal: !prevState.showModal
      };
    });
  }

  render() {

    if (this.props.appointments.size === 0) {
      return null;
    }

    return (
      <section id="main" className="Appointments ">
        <ul>
          {[...this.props.appointments.values()].map(appointment => (
            <AppointmentTile {...this.props} key={appointment.id} appt={appointment} handler={this.handleApptClick} />
          ))}
        </ul>

        <AppointmentModal name={this.state.name} phone={this.state.phone} timeSlot={this.state.timeSlot} handleSubmit={this.handleSubmit}
          handleNameChange={this.handleNameChange} handlePhoneChange={this.handlePhoneChange}
          handleClose={this.toggleModal} showModal={this.state.showModal} modalError={this.state.modalError} />

      </section>
    );
  }
}

function AppointmentModal (props) {

  return (
    <section className={"AppointmentModal " + (props.showModal ? "show-modal" : "hide-modal")} >
      <img src={closeIcon} onClick={props.handleClose} />

      <h3>{props.timeSlot}</h3>

      <input type="input"
        value={props.name}
        placeholder="Name"
        onChange={props.handleNameChange} />

      <input type="input"
        value={props.phone}
        placeholder="Phone"
        onChange={props.handlePhoneChange} />

      <p className={props.modalError ? "error-message-visible" : "error-message-hidden"}>Please enter a standard 10-digit phone number</p>

      <button className="submit" onClick={props.handleSubmit}> Claim Appointment </button>

    </section>
  );
}

function AppointmentTile (props) {

  return (
    <li className={"AppointmentTile " + props.className + (props.appt.occupied ? " occupied" : " available")}
      onClick={props.handler} data-appt-id={props.appt.id}
      data-appt-name={props.appt.name} data-appt-phone={props.appt.phone}
      data-appt-timeslot={props.appt.timeSlot}>

      <div className="view">

        <label>{props.appt.timeSlot}</label>
      </div>
    </li>
  );
}


export default Appointments;
