import React from 'react';
import ReactDOM from 'react-dom';
import AppointmentContainer from './containers/AppointmentContainer';

//ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(<AppointmentContainer />, document.getElementById('root'));


import AppointmentActions from './data/AppointmentActions';

AppointmentActions.addAppt('9:00AM');
AppointmentActions.addAppt('10:00AM');
AppointmentActions.addAppt('11:00AM');
AppointmentActions.addAppt('12:00PM');
AppointmentActions.addAppt('1:00PM');
AppointmentActions.addAppt('2:00PM');
AppointmentActions.addAppt('3:00PM');
AppointmentActions.addAppt('4:00PM');
AppointmentActions.addAppt('5:00PM');
