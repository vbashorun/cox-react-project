import Immutable from 'immutable';
import {ReduceStore} from 'flux/utils';
import AppointmentActionTypes from './AppointmentActionTypes';
import AppointmentDispatcher from './AppointmentDispatcher';
import Appointment from './Appointment';
import Counter from './Counter';

class AppointmentStore extends ReduceStore {
  constructor() {
    super(AppointmentDispatcher);
  }

  getInitialState() {
    return Immutable.OrderedMap();
  }

  reduce(state, action) {
    switch (action.type) {

      case AppointmentActionTypes.ADD_APPT:

        // Don't add appts with no time.
        if (!action.timeSlot) {
          return state;
        }

        const id = Counter.increment();

        return state.set(id, new Appointment({
          id,
          timeSlot: action.timeSlot,
          occupied: false,
        }));

      case AppointmentActionTypes.UPDATE_APPT:

        let appt = {},
        prev = state.get(action.id);

        appt.id = prev.id;
        appt.name = action.name.length > 0 ? action.name : "";
        appt.phone = action.phone;
        appt.timeSlot = prev.timeSlot;
        appt.occupied = appt.name.length > 0 && appt.phone.length > 0 ? true : false;

        return state.set(appt.id, appt);

      default:
        return state;
    }
  }
}

export default new AppointmentStore();
