import Immutable from 'immutable';

const Appointment = Immutable.Record({
  id: '',
  name: '',
  phone: '',
  occupied: false,
  timeSlot: ''
});

export default Appointment;
