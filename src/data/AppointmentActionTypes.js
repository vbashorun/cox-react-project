const ActionTypes = {
  ADD_APPT: 'ADD_APPT',
  UPDATE_APPT: 'UPDATE_APPT',
};

export default ActionTypes;
