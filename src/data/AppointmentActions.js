import AppointmentActionTypes from './AppointmentActionTypes';
import AppointmentDispatcher from './AppointmentDispatcher';

const Actions = {
  addAppt(timeSlot) {
    AppointmentDispatcher.dispatch({
      type: AppointmentActionTypes.ADD_APPT,
      timeSlot,
    });
  },

  updateAppt(id, name, phone) {
    AppointmentDispatcher.dispatch({
      type: AppointmentActionTypes.UPDATE_APPT,
      id,
      name,
      phone
    });
  },

};

export default Actions;
