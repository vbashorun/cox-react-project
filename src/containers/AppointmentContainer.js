import AppView from '../components/AppView/AppView';
import {Container} from 'flux/utils';
import AppointmentStore from '../data/AppointmentStore';
import AppointmentActions from '../data/AppointmentActions';

function getStores() {
  return [
    AppointmentStore,
  ];
}

function getState() {
  return {
    appointments: AppointmentStore.getState(),
    onUpdate: AppointmentActions.updateAppt,
  };
}

export default Container.createFunctional(AppView, getStores, getState);
