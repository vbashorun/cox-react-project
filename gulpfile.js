'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass'),
shell = require('gulp-shell'),
autoprefixer = require('gulp-autoprefixer');

// starts development server on localhost:3000
gulp.task('start-server', shell.task([
  'npm run start'
]));


// JSX preprocessor/compiler - runs webpack
gulp.task('build-project', shell.task([
  'npm run build'
]));

gulp.task('js-watch', function () {
    gulp.watch(['./src/index.js', './src/components/**/*.js'], ['build-project']);
});


// CSS/Styling
gulp.task('sass-watch', function () {
  gulp.watch(['./src/styles.scss','./src/**/*.scss'], ['sass-compile', 'build-project']);
});

gulp.task('sass-compile', function () {
    gulp.src('./src/components/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
    .pipe(gulp.dest('./src/components'));
});

gulp.task('default', ['sass-compile', 'build-project', 'sass-watch', 'js-watch', 'start-server'], function() {
  //empty task for running above sequence
});
