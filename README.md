## Available Scripts

In the project directory, you can run:

### `npm install`
Installs project dependencies

### `npm run build`
runs webpack and outputs bundle file in path determined in webpack.config.js

The page will reload if you make edits.<br>

### `npm run start`
Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


## The App
A simple project that provides a list of appointments. Available appointments are green
while occupied appointments are red. Clicking on an appointment, brings up a form that
shows who owns the appointment, if there's an owner. An appointment only counts as 'occupied' if there is both
a name and valid phone number.
